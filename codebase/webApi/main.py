from flask import Blueprint
from flask import Flask
from flask import json
from flask import request
from bson.json_util import dumps
from flask_cors import CORS,cross_origin
import json
from codebase.extensions import mongo
from codebase.utility import *

main = Blueprint('main', __name__)
CORS(main)

#TestConnectivity
@main.route('/')
def hello():
    return "Test Connectivity Successful"

#Returning Random User
@main.route('/users')
def getAll():
    user_collection = mongo.db.hackcollection
    return dumps(user_collection.find_one())

@main.route('/user/<email>')
def profile(email):
    user_collection = mongo.db.hackcollection
    myquery = { "email": email}
    return dumps(user_collection.find(myquery))

@main.route('/chat/register', methods=['POST','PUT'])
def registerForChatDb():
    chatRegistry = mongo.db.chatRegistry
    if chatRegistry.find({"user1" : request.json["user1"], "user2" : request.json["user2"]}).count() is 0:
        chatRegistry.insert(createChatRegistyObjectForDataBase(request.json))
        return json.dumps(request.json)
    else:
        return "user pair is already in registry"

@main.route('/chat/get/', methods=['GET'])
def getAllChat():
    chatRegistry = mongo.db.chatRegistry
    return dumps(chatRegistry.find())
