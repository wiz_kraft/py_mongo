from flask import Blueprint
from codebase.extensions import mongo
from codebase.utility import *
import json
from flask import request
from bson.json_util import dumps
from flask_cors import CORS


getUsers = Blueprint('getUsers', __name__)
CORS(getUsers)

#Get Blood Group, Location Bases Users
@getUsers.route('/getUsers', methods=['GET'])
def getLocBasedUsers():
    userDatabase = mongo.db.UserRegisterData

    #radius needs to be passed in kilometres
    cor,radius,bloodGroup = parseGetUserJson(request.json)

    myquery = { "location": { "$geoWithin": { "$center": [ [cor[0], cor[1]], radius/111.3 ] } } , "bloodGroup" : bloodGroup, "volunteerToDonate" : "True"}
    return (dumps(userDatabase.find(myquery)))