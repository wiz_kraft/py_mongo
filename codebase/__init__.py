from flask import Flask 
from flask_cors import CORS
from .extensions import mongo

import sys
sys.path.append('codebase/webApi')
from signup import signup_page
from getUsers import getUsers
from main import main


def create_app(config_object='codebase.settings'):
    app = Flask(__name__)

    CORS(app)
    
    app.config.from_object(config_object)

    mongo.init_app(app)

    app.register_blueprint(main)

    app.register_blueprint(signup_page)

    app.register_blueprint(getUsers)

    return app