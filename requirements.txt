click==6.7
Flask==1.0.2
itsdangerous==0.24
Jinja2==2.10
MarkupSafe
Werkzeug==0.14.1
flask_pymongo
python-dotenv
dnspython
flask-cors